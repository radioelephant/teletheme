# Teletheme Builder Readme
This Project helps you with building Telegram themes on Windows.

## Requirements
This Project requires a Git-Tool with which you can clone the repository. For example `GIT-SCM`, klick [here to download](https://git-scm.com/downloads) it.
Make sure to install it globally, so you can use it in any directory.
Further more you need a Computer running any version of Windows.
Finally you need Telegram Desktop which you can get [here](https://desktop.telegram.org).

## Setup
To get started simply clone the project to your local workspace.
To do that open a CMD (by pressing WINDOWS + R and then entering `cmd` and then press ENTER) and navigate to the directory where you want the project to be cloned to (f.ex. `cd C:\git-projects\`).
Then you type in `git clone https://gitlab.com/radioelephant/teletheme.git` and start it by pressing ENTER.
You're all done.

## Usage
Open a CMD and navigate to the folder where you cloned the project to and enter the project folder called `teletheme`.
If you just did the setup you only need to enter the directory `teletheme`.
To create a new style type in `build new` and follow the instructions (or simply enter `build new yourThemenameHere` to generate a new theme with your desired name).
Navigate into the new directory by the name of your new style.
Edit the `colors.tdesktop-theme` file to your likings.
The colors in this file are noted in the HEX notation for colors (#RRGGBB(AA)).
If you don't know about HEX Color notation read [this explanation on w3schools](http://www.w3schools.com/colors/colors_hexadecimal.asp). If you're confused about the 8 digit notation read [this answer on stackoverflow](http://stackoverflow.com/questions/7015302/css-hexadecimal-rgba#answer-27802062).
When you are ready to try it out go back to the parent direcory of your style called `teletheme` and run `build` or `build yourThemenameHere` from there to build your theme.
After doing that you can grab the file `yourThemeName.tdesktop-theme` and send it to yourself on Telegram.
Finally you open the sent style in Telegram and click on apply this theme to use it as your Telegram Desktop theme.
If you want to share your theme with other people who have this project, go ahead an do the following in the teletheme directory: `git add .` `git commit -m "Your commit message including your themename"` `git push`.
Then your changes to this project should be on [GitLab](https://gitlab.com/radioelephant/teletheme) for eveyone to see.
If you need help you can try `build /?` for more information in your commandline.

## FAQ
No questions have been asked so far.


## Need Help?
Hit me up on [Telegram @RobinMeier](telegram.me/RobinMeier). I am happy to help you.


## Licence
You can use, modify and redistribute this code freely just please include a link to this repository on GitLab.
Make sure to add yourself to the contributors in the README.md file if you contribute to this project.

## Contributors
 - [Radioelephant on GitLab](https://gitlab.com/radioelephant) | [Robin Meier on Telegram](telegram.me/RobinMeier)

