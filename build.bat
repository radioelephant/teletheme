@ECHO OFF

REM Buildscript for Telegram Desktop Theme
REM Written by t.me/RobinMeier
REM You can use, modify and redistribute this code freely
REM but please include this link in your description
REM https://gitlab.com/radioelephant/teletheme.git

SETLOCAL

IF [%1]==[] (
	SET /P Themename=Enter the name of your style: 
) ELSE (
	SET Themename=%1
)

IF [%Themename%]==[new] (
	GOTO :Createnew
) ELSE IF [%Themename%]==[/?] (
	GOTO :Helpsection
) ELSE (
	GOTO :Buildscript
)


:Buildscript


IF NOT EXIST %~dp0%Themename% (
	GOTO :ErrorThemeNotFound
)

IF [%2]==[] (
	SET Version=%RANDOM%
	SET IsRandom=1
) ELSE (
	SET Version=%2
	SET IsRandom=0
)

ECHO Themename: %Themename%
ECHO Version: %Version%
ECHO Is Random Number for Version: %IsRandom%

ECHO Generate zip with needed files
7za a ./1_BUILDS/%Themename%-%Version%.zip ./%Themename%/colors.tdesktop-theme ./%Themename%/tiled.png ./%Themename%/background.png ./%Themename%/tiled.jpg ./%Themename%/background.jpg

COPY /Y .\1_BUILDS\%Themename%-%Version%.zip .\%Themename%\%Themename%.tdesktop-theme

IF EXIST %~dp0%themename%\all_versions (
REM THIS DOES NOT WORK
	ECHO Directory all_versions does exist
	ECHO %~dp0%themename%\all_versions
) ELSE (
	ECHO Directory all_versions does not exist
	ECHO %~dp0%themename%\all_versions
	MKDIR .\%themename%\all_versions
	ECHO Directory has beed created
)

IF %IsRandom%==1 (
	ECHO Random was on
	COPY /Y .\1_BUILDS\%Themename%-%Version%.zip .\%Themename%\all_versions\%Themename%-B%Version%.tdesktop-theme
) ELSE (
	ECHO Random was off
	COPY /Y .\1_BUILDS\%Themename%-%Version%.zip .\%Themename%\all_versions\%Themename%-V%Version%.tdesktop-theme
)

GOTO End

:Createnew

IF [%2]==[] (
	SET /P Newthemename=Enter your desired theme name: 
) ELSE (
	SET Newthemename=%2
)

REM THIS DOES NOT WORK
IF EXIST %~dp0%Newthemename% (
	PAUSE
	ECHO it does exist
	ECHO %~dp0%Newthemename%
	GOTO :ErrorThemeAlreadyExists
) ELSE (
	PAUSE
	ECHO it does not exist: %~dp0%Newthemename%
)

ECHO Creating directory
MKDIR %Newthemename%

ECHO Copying default files
COPY /-Y .\1_SAMPLE\colors.tdesktop-theme .\%Newthemename%\colors.tdesktop-theme
COPY /-Y .\1_SAMPLE\tiled.jpg .\%Newthemename%\tiled.jpg

ECHO ALL DONE!


ECHO A theme folder has been created for you.

GOTO End

:Helpsection

ECHO Welcome to the Helpsection
ECHO Basic usage of this batch file:
ECHO 	build			run buildscript, enter parameters inline
ECHO 	build [themename]	build your theme
ECHO 	build new [themename]	create a new theme

GOTO End

:ErrorThemeNotFound

ECHO The specified theme could not be found. Aborting...

GOTO End

:ErrorThemeAlreadyExists

ECHO A theme with the specified name already exists. Aborting...

GOTO End

:End